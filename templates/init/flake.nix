{
  description = "EC Toolbox Flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    systems.url = "github:nix-systems/default";
    ec-lib.url = "git+https://code.europa.eu/pol/ec-lib.git";
  };

  outputs =
    inputs@{ flake-parts, systems, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      systems = import systems;

      perSystem =
        { system, ... }:
        {
          _module.args.pkgs = import inputs.nixpkgs {
            inherit system;
            overlays = [ inputs.ec-lib.overlays.default ];
          };

          # The available function are available now under `pkgs.ec.*`
        };
    };
}
