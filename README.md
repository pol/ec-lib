# European Commission Library

Unofficial European Commission Nix flake toolbox.

Provides specific tools and utilities used at and for the European Commission.

## Usage

1. Add the `default` overlay in your own flake
2. Available functions and packages are available under the `ec` namespace

## Getting started

This project has a `init` template to get you started quickly:

```shell
nix flake init --template 'git+https://code.europa.eu/pol/ec-lib.git#init'
```

## Documentation

### `ec.dockerTools.buildImage`

Creates an OCI container which embed the SBOM of its contents in its metadata.
OpenContainers Annotations attributes are supported.

### `ec.dockerTools.buildLayeredImage`

Creates an OCI container which embed the SBOM of its contents in its metadata.
OpenContainers Annotations attributes are supported.

#### Usage:

1. Declare the image with `pkgs.ec.dockerTools.buildLayeredImage`

```nix
my-oci-container = pkgs.ec.dockerTools.buildLayeredImage {
  name = "my-oci-container";
  tag = "latest";
  authors = "John Doe";
  description = "A simple OCI container";
  version = "1.0.0";

  # ... rest of the image attributes here under...
};
```

2. Build the image

```shell
nix build .#my-oci-container
```

3. Load the image in Docker or Podman

```shell
docker load < result
```

4. Inspect the image metadata

```shell
docker inspect my-oci-container:latest
```

5. Extract the SBOM in CycloneDX format with:

```shell
docker inspect my-oci-container:latest | jq '.[0].Config.Labels."SBOM"' | jq
```
