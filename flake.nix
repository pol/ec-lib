{
  description = "European Commission Toolbox Library";

  inputs = {
    bombon.url = "github:nikstur/bombon";
  };

  outputs =
    inputs@{ self, ... }:
    {
      overlays.default =
        final: prev:
        let
          commit = self.rev or "dirty";
          logo = ''
            ${(builtins.readFile ./resources/logo.txt)}
            Unofficial European Commission Toolbox Library${
              if commit == "dirty" then "" else " (commit: ${commit})"
            }
            More info at https://code.europa.eu/pol/ec-lib
          '';
        in
        {
          ec = (builtins.trace (logo) (prev.pkgs.callPackage ./src/lib.nix { inherit inputs final prev; }));
        };

      templates.init = {
        path = ./templates/init;
        description = "A basic init template for getting started with this project";
      };
    };
}
