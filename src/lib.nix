{
  inputs,
  lib,
  prev,
  ...
}:

let
  # Documentation: https://specs.opencontainers.org/image-spec/annotations/
  openContainersImageSuffixes = [
    "authors"
    "created"
    "description"
    "documentation"
    "licences"
    "revision"
    "source"
    "title"
    "url"
    "vendor"
    "version"
  ];
  cleanedArgs = args: builtins.removeAttrs args openContainersImageSuffixes;
  openContainersLabels =
    args:
    builtins.listToAttrs (
      builtins.filter (set: set.value != null) (
        builtins.map (suffix: {
          name = "org.opencontainers.image.${suffix}";
          value = args."${suffix}" or null;
        }) openContainersImageSuffixes
      )
    );
in
{
  dockerTools.buildLayeredImage =
    args@{ ... }:
    prev.pkgs.dockerTools.buildLayeredImage (
      lib.attrsets.recursiveUpdate (cleanedArgs args) {
        config.Labels = (openContainersLabels args) // {
          SBOM =
            let
              sbomContent =
                if builtins.isList args.contents then
                  prev.pkgs.symlinkJoin {
                    inherit (args) name;
                    tag = args.tag;
                    paths = args.contents;
                  }
                else
                  args.contents;
            in
            (builtins.readFile (inputs.bombon.lib.${prev.pkgs.system}.buildBom sbomContent { }));
        };
      }
    );
  dockerTools.buildImage =
    args@{ ... }:
    prev.pkgs.dockerTools.buildImage (
      lib.attrsets.recursiveUpdate (cleanedArgs args) {
        config.Labels = (openContainersLabels args) // {
          SBOM = (builtins.readFile (inputs.bombon.lib.${prev.pkgs.system}.buildBom args.copyToRoot { }));
        };
      }
    );
}
